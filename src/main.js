import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import ChildComponent from './ChildComponent'
import routes from '@/routes'
import EventBus from '@/plugins/event-bus'
Vue.use(VueRouter)
Vue.use(EventBus)

const router = new VueRouter({ routes })

Vue.component('child', ChildComponent)
new Vue({
  el: '#app',
  render: h => h(App),
  router
})
